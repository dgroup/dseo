from setuptools import setup, find_packages


setup(
    name='dseo',
    version='0.1dev',
    license='BSD',
    long_description=open('README.md').read(),
    packages=find_packages(),
    include_package_data=True,
    package_data={
        'dseo': ['templates/**/*.html']
    },
)
